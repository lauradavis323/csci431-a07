suspect(twoNuns).
suspect(m).
suspect(victim).
suspect(drunk).
suspect(personSeen).
suspect(other).

weapon(fiveSmallKnives).
weapon(otherKnives).
weapon(bottleShards).

foundAtScene(bottleShards).

0.4::saw(drunk, personSeen).
0.89::dropped(durnk, bottleShards).

0.68::notSeen(m).
0.9::disapearsRegularly(m).
expectedHome(m).
missing(X) :- notSeen(X), disapearsRegularly(X), expectedHome(X).

bodyFound(twoNuns, victim).

0.01::causeOfDeath(suicide).
0.99::causeOfDeath(stabbing).

0.89::hourOfDeath(midnight).
0.9::saitanicSymbol(pentagram).
0.99::blood(victim).
locationOfSymbol(churchWall).
saitanicDeath(victim) :- hourOfDeath(midnight), saitanicSymbol(pentagram), blood(victim), locationOfSymbol(churchWall).

weaponBelongsTo(fiveSmallKnives, twoNuns).
0.78::weaponBelongsTo(bottleShards, drunk).
0.5::weaponBelongsTo(otherKnives, X) :- suspect(X).
0.53::weaponCleanedBecauseMurder(fiveSmallKnives).

wroteSatanicLiterature(m).
0.95::ownSatanicLiterature(drunk).

0.05::isSatanist(twoNuns).
0.78::isSatanist(m) :- suspect(m), wroteSatanicLiterature(m).
isSatanist(drunk) :- suspect(drunk), ownSatanicLiterature(drunk).
0.1::isSatanist(X) :- suspect(X).

victim(m) :- missing(m), causeOfDeath(suicide).
victim(victim) :- \+victim(m).

murderer(victim) :- suspect(victim), causeOfDeath(suicide).
murderer(m) :- suspect(m), victim(m), \+missing(m).
murderer(twoNuns) :- suspect(twoNuns), weaponBelongsTo(X, twoNuns), weaponCleanedBecauseMurder(X), murderWeapon(X), causeOfDeath(stabbing), saitanicDeath(_), isSatanist(twoNuns).

murderer(drunk) :- suspect(drunk), causeOfDeath(stabbing), weaponBelongsTo(X, drunk), murderWeapon(X), saitanicDeath(_), isSatanist(drunk), \+saw(drunk, _).

murderer(personSeen) :- suspect(personSeen), causeOfDeath(stabbing), weaponBelongsTo(X, personSeen), murderWeapon(X), saitanicDeath(_), isSatanist(personSeen), saw(_, personSeen).

murderer(other) :- suspect(other), causeOfDeath(stabbing), weaponBelongsTo(X, other), murderWeapon(X), saitanicDeath(_), isSatanist(other).

murderWeapon(fiveSmallKnives) :- suspect(X), weaponBelongsTo(fiveSmallKnives, X), weaponCleanedBecauseMurder(fiveSmallKnives).
murderWeapon(bottleShards) :- suspect(X), weaponBelongsTo(bottleShards, X), dropped(X, bottleShards).
murderWeapon(otherKnives) :- suspect(X), weaponBelongsTo(otherKnives, X).

query(victim(X)).
query(murderer(X)).
query(murderWeapon(X)).
